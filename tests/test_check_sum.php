<?php
require_once __DIR__ . '/../functions.php';

function testGetSum(): void
{
    $sum = array_sum(getSum());

    if ($sum > 100) {
        throw new OutOfRangeException('Sum cannot be greater than 100. Current sum is : ' . $sum . PHP_EOL);
    }
}

testGetSum();